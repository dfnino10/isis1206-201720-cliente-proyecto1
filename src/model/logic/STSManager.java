package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.annotation.Repeatable;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.xml.bind.ValidationEvent;

import com.google.gson.Gson;

import API.ISTSManager;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.QueueList;
import model.data_structures.Stack;
import model.vo.BusUpdateVO;
import model.vo.StopEstimService;
import model.vo.VOParada;
import model.vo.VORetardo;
import model.vo.VORetardoViaje;
import model.vo.VORuta;
import model.vo.VOServicio;
import model.vo.VOStopTimes;
import model.vo.VOPlan;
import model.vo.VORangoHora;
import model.vo.VOTransfer;
import model.vo.VOViaje;
import model.vo.VOPlan.ParadaPlanVO;

public class STSManager implements ISTSManager
{

	private static String STOPS="src/gtfs-8-4/stops.txt";
	private static String STOP_TIMES="src/gtfs-8-4/stop_times.txt";
	private static String ROUTES="src/gtfs-8-4/routes.txt";
	private static String TRIPS="src/gtfs-8-4/trips.txt";
	private static String CALENDAR_DATES="src/gtfs-8-4/calendar_dates.txt";
	private static String CALENDAR="src/gtfs-8-4/calendar.txt";
	private static String TRANSFERS="src/gtfs-8-4/transfers.txt";
	private static String SHAPES="src/gtfs-8-4/shapes.txt";
	private static String BUS_SERVICE0821="./RealTime-8-21-BUSES_SERVICE";

	//Empresas de transporte de Vancouver
	private static String[] CMBC={"CMBC","TransLink"};
	private static String[] WCEX={"WCEX","West Coast Express"};
	private static String[] SKYT={"SKYT","British Columbia Rapid Transit Company"};


	private DoubleLinkedList<VORuta> listaRoutes;
	private DoubleLinkedList<VOParada> listaStops;
	private DoubleLinkedList<VOViaje> listaViajes;
	private BusUpdateVO[] busUpdates;
	private DoubleLinkedList<VOStopTimes> listaStopTimes;

	@Override
	public void ITSInit() 
	{
		/*listaRoutes=new DoubleLinkedList<VORuta>();
		listaStops=new DoubleLinkedList<VOParada>();*/

	}

	@Override
	public void ITScargarGTFS() 
	{
		loadRoutes(ROUTES);
	}

	@Override
	public void ITScargarTR(String fecha) 
	{
		String fe=fecha.substring(4,6);//20170625
		if(fe.charAt(0)==0){fe=fe.substring(1);}
		String cha=fecha.substring(6);
		String archivoBus= "./RealTime"+"-"+fe+"-"+cha+"-"+"BUSES_SERVICE";
		String archivoStop= "./RealTime"+"-"+fe+"-"+cha+"-"+"STOPS_ESTIM_SERVICES";

		File f = new File(archivoBus);
		File[] updateFiles = f.listFiles();

		File s = new File(archivoStop);
		File[] updFiles = s.listFiles();

		for (int i = 0; i < updateFiles.length; i++) 
		{
			try 
			{
				readBusUpdate(updateFiles[i]);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		for (int i = 0; i < updFiles.length; i++) 
		{
			try 
			{
				readStopEstimService(updFiles[i]);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// TODO Falta retornar la lista ordenada
	@Override
	public IList<VORuta> ITSrutasPorEmpresa(String nombreEmpresa, String fecha) throws Exception
	{
		String empresa=null;
		DoubleLinkedList<VORuta> rutas= new DoubleLinkedList<VORuta>();
		DoubleLinkedList<VORuta> rutasRetornar= new DoubleLinkedList<VORuta>();

		if(nombreEmpresa.equalsIgnoreCase(CMBC[0])||nombreEmpresa.equalsIgnoreCase(CMBC[1]))
			empresa=CMBC[0];

		else if(nombreEmpresa.equalsIgnoreCase(WCEX[0])||nombreEmpresa.equalsIgnoreCase(WCEX[1]))
			empresa=WCEX[0];

		else if(nombreEmpresa.equalsIgnoreCase(SKYT[0])||nombreEmpresa.equalsIgnoreCase(SKYT[1]))
			empresa=SKYT[0];

		else 
		{
			throw new Exception("La empresa con el nombre dado no fue encontrada");
		}

		for (int i = 0; i < listaRoutes.size(); i++) 
		{
			VORuta ruta = listaRoutes.darElementoPos(i);
			if(ruta.getAgencyId().equalsIgnoreCase(empresa))
			{
				rutas.addLast(ruta);
			}
		}

		for (int j = 0; j < rutas.size(); j++) 
		{
			VORuta rutaN = rutas.darElementoPos(j);
			if(rutaDisponibleEnFecha(rutaN,fecha))
			{
				rutasRetornar.addLast(rutaN);
			}			
		}
		return rutasRetornar;
	}

	@Override
	public IList<VOViaje> ITSviajesRetrasadosRuta(String idRuta, String fecha) 
	{
		DoubleLinkedList<VOViaje> viajes = new DoubleLinkedList<VOViaje>();
		try 
		{
			VORuta r =null;
			for (int i = 0; i < listaRoutes.size(); i++)
			{
				VORuta ruta = listaRoutes.darElementoPos(i);
				if(Integer.parseInt(ruta.getIdRoute())==Integer.parseInt(idRuta))
				{
					r=ruta;
					break;
				}
			}

			for (int i = 0; i < r.getlistaTrips().size(); i++) 
			{
				VOViaje viaje =r.getlistaTrips().darElementoPos(i);
				if(viaje.isRetardo())
				{
					viajes.addLast(viaje);
				}
			}

		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return viajes;
	}

	//TODO Ordenar la lista a retornar
	@Override
	public IList<VOParada> ITSparadasRetrasadasFecha(String fecha) 
	{
		DoubleLinkedList<VOParada> retornar = new DoubleLinkedList<VOParada>();
		try 
		{
			for (int i = 0; i < listaStops.size(); i++) 
			{
				VOParada parada = listaStops.darElementoPos(i);
				if(parada.getNumeroIncidentes()>0)
				{
					retornar.addLast(parada);
				}
			}
			return retornar;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public IList<VOTransfer> ITStransbordosRuta(String idRuta, String fecha)
	{
		Stack<VOTransfer> retornar = new Stack<VOTransfer>();

		DoubleLinkedList<VORuta> listaAux= new DoubleLinkedList<VORuta>();


		try 
		{
			VORuta rutaBuscada = null;
			for (int i = 0; i < listaRoutes.size(); i++) 
			{
				VORuta ruta= listaRoutes.darElementoPos(i);
				if(Integer.parseInt(ruta.getIdRoute())==Integer.parseInt(idRuta))
				{
					rutaBuscada=ruta;
					break;
				}
			}
			if(rutaBuscada!=null && rutaDisponibleEnFecha(rutaBuscada, fecha))
			{
				listaAux.addFirst(rutaBuscada);
				for (int i = 0; i < rutaBuscada.getlistaParadas().size(); i++) 
				{
					VOParada parada = rutaBuscada.getlistaParadas().darElementoPos(i);
					for (int j = 0; j < parada.getListaTransbordos().size(); j++) 
					{
						VOTransfer transbordo = parada.getListaTransbordos().darElementoPos(j);
						ITStransbordosRuta(listaAux, transbordo, listaStops, parada);
						retornar.addFirst(transbordo);
					}
				}
			}
			else
			{
				throw new Exception("La ruta no est� disponible en la fecha dada");
			}

		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return retornar;
	}
	public IList<VOTransfer> ITStransbordosRuta(DoubleLinkedList<VORuta> rutas, VOTransfer transfer, DoubleLinkedList<VOParada> listaParadasOriginal,VOParada pParada)
	{
		Stack<VOTransfer> retornar = new Stack<VOTransfer>();

		try 
		{
			for (int i = 0; i < transfer.getListadeParadas().size(); i++) 
			{
				VOParada parada = transfer.getListadeParadas().darElementoPos(i);
				if(!parada.equals(pParada))
				{
					VORuta rutaN=parada.getRutas().darElementoPos(i);
					for (int j = 0; j < rutas.size(); j++) 
					{
						if(!rutaN.equals(rutas.darElementoPos(j)))
						{
							rutas.addFirst(rutaN);
							for (int k = 0; k < rutaN.getlistaParadas().size(); k++) 
							{
								VOParada parada1= rutaN.getlistaParadas().darElementoPos(k);
								if(!parada1.equals(parada))
								{
									for (int l = 0; l < parada1.getListaTransbordos().size(); l++) 
									{
										VOTransfer trans = parada1.getListaTransbordos().darElementoPos(l);
										ITStransbordosRuta(rutas, trans, rutaN.getlistaParadas(), parada1);
										retornar.addFirst(trans);
									}															
								}
							}						
						}
					}
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return retornar;
	}

	@Override
	public VOPlan ITSrutasPlanUtilizacion(IList<String> idsDeParadas, String fecha, String horaInicio, String horaFin) 
	{
		try 
		{
			QueueList<VOParada> listaParadas =new QueueList<VOParada>();
			DoubleLinkedList<VORuta> rutas = new DoubleLinkedList<VORuta>();
			DoubleLinkedList<VOViaje> viajes= new DoubleLinkedList<VOViaje>();
			
			for (int i = 0; i < idsDeParadas.size(); i++) 
			{
				String pid = idsDeParadas.darElementoPos(i);
				
				for (int j = 0; j < listaStops.size(); j++) 
				{
					VOParada parada = listaStops.darElementoPos(j);
					if(parada.getStopId()==Integer.parseInt(pid))
					{
						listaParadas.addFirst(parada);
					}
				}
			}
			
			for (int i = listaStops.size()-1; 0>=i ; i--) 
			{
				VOParada p= listaParadas.darElementoPos(i);
				for (int j = 0; j < p.getRutas().size(); j++) 
				{
					VORuta ruta = p.getRutas().darElementoPos(j);
					rutas.addLast(ruta);
				}
				for (int j = 0; j < p.getListaViajes().size(); j++) 
				{
					VOViaje viaje = p.getListaViajes().darElementoPos(j);
					viajes.addLast(viaje);
				}
			}
			
			VOPlan plan = new VOPlan();
			
			for (int i = 0; i < listaParadas.size(); i++) 
			{
				ParadaPlanVO paradaP= plan.new ParadaPlanVO(""+listaParadas.darElementoPos(i).getStopId());
				plan.getSecuenciaDeParadas().addLast(paradaP);
			}
			
			for (int i = 0; i < plan.getSecuenciaDeParadas().size(); i++) 
			{
				ParadaPlanVO parada=plan.getSecuenciaDeParadas().darElementoPos(i);
				for (int j = 0; j < parada.getRutasAsociadasAParada().size(); j++) 
				{
					
				}
			}
			
			
		}
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public IList<VORuta> ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha) 
	{
		try {
			DoubleLinkedList<VORuta> rutasEmpresa = new DoubleLinkedList<VORuta>();
			String empresa;
			if(nombreEmpresa.equalsIgnoreCase(CMBC[0])||nombreEmpresa.equalsIgnoreCase(CMBC[1]))
				empresa=CMBC[0];

			else if(nombreEmpresa.equalsIgnoreCase(WCEX[0])||nombreEmpresa.equalsIgnoreCase(WCEX[1]))
				empresa=WCEX[0];

			else if(nombreEmpresa.equalsIgnoreCase(SKYT[0])||nombreEmpresa.equalsIgnoreCase(SKYT[1]))
				empresa=SKYT[0];
			else
			{
				throw new Exception("No existe la empresa");
			}
			for (int i = 0; i < listaRoutes.size(); i++) 
			{
				VORuta ruta = listaRoutes.darElementoPos(i);
				if(ruta.getAgencyId().equalsIgnoreCase(empresa) && rutaDisponibleEnFecha(ruta, fecha))
				{
					rutasEmpresa.addLast(ruta);
				}
			}
			int n = rutasEmpresa.size();
			int h=1;
			while (h<n/3)
			{
				h=3*h+1;
			}
			while(h>0)
			{
				for (int i = h; i < n; i++) 
				{
					boolean termino=false;
					for (int j = i; j >=h&&!termino; j-=h) 
					{
						VORuta ruta = rutasEmpresa.darElementoPos(j);
						VORuta ruta2= rutasEmpresa.darElementoPos(j-h);
						if (ruta2.getlistaParadas().size()>ruta.getlistaParadas().size())
						{
							termino=true;
						}
						else
						{
							rutasEmpresa.exch(ruta, ruta2);
						}
					}
				}
				h=h/3;
			}
			return rutasEmpresa;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public IList<VOViaje> ITSviajesRetrasoTotalRuta(String idRuta, String fecha)
	{
		// TODO Auto-generated method stub
		DoubleLinkedList<VOViaje> viajes = new DoubleLinkedList<VOViaje>();
		try 
		{
			VORuta ruta =null;
			boolean ya=false;
			for (int i = 0; i < listaRoutes.size() && !ya; i++)
			{
				ruta = listaRoutes.darElementoPos(i);
				if(ruta.getIdRoute().equalsIgnoreCase(idRuta))
				{
					ya=true;
				}
			}
			VOViaje viaje =null;
			boolean ya2=false;
			int pos=0;
			for (int i = 0; i < ruta.getlistaTrips().size() && !ya2; i++) 
			{
				viaje =ruta.getlistaTrips().darElementoPos(i);
				if(viaje.isRetardo())
				{
					viajes.addFirst(viaje);
					ya2=true;
					pos=i;
					if (ya2)
					{
						boolean noFueUna=false;
						for (int j = pos; j < ruta.getlistaTrips().size() && !noFueUna; j++) 
						{
							VOViaje siguiente = ruta.getlistaTrips().darElementoPos(j);
							if (!siguiente.isRetardo())
							{
								noFueUna=true;
							}
							else
							{
								viajes.addLast(siguiente);
							}
						}
					}
				}
			}

			//TODO organizar
		} 

		catch (Exception e) {
			e.printStackTrace();
		}
		return viajes;
	}

	@Override
	public VORangoHora ITSretardoHoraRuta(String idRuta, String fecha) 
	{
		try {
			DoubleLinkedList<VOViaje> listaViajesRetrasados = (DoubleLinkedList<VOViaje>) ITSviajesRetrasadosRuta(idRuta, fecha);
			int maxRetardos=0;
			VORangoHora mayor=null;
			DoubleLinkedList<VORangoHora> horas = new DoubleLinkedList<VORangoHora>();
			for (int i = 0; i < 24; i++) 
			{
				VORangoHora rango = new VORangoHora();
				rango.setHoraInicial(i);
				rango.setHoraFinal(i+1);
				horas.addLast(rango);
			}
			for (int j = 0; j < horas.size(); j++) 
			{
				VORangoHora hora = horas.darElementoPos(j);
				IList<VORetardoViaje> listaRetardos = new DoubleLinkedList<VORetardoViaje>();
				for (int i = 0; i < listaViajesRetrasados.size(); i++)
				{
					VOViaje viaje = listaViajesRetrasados.darElementoPos(i);
					if(viaje.getHoraRetardo()==hora.getHoraInicial())
					{
						VORetardoViaje ret = new VORetardoViaje(Integer.parseInt(viaje.getIdViaje()), viaje.getHoraRetardo());
						listaRetardos.addFirst(ret);
					}
				}
				hora.setListaRetardos(listaRetardos);
				if (listaRetardos.size()>maxRetardos)
				{
					maxRetardos=listaRetardos.size();
					mayor=hora;
				}
			}
			return mayor;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public IList<VOViaje> ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaInicio, String horaFin)  
	{
		// TODO Auto-generated method stub
		try {
			String[] hora = horaInicio.split(":");
			IList<VOViaje> viajes = new DoubleLinkedList<VOViaje>(); 
			for (int i = 0; i < listaRoutes.size(); i++)
			{
				VORuta ruta = listaRoutes.darElementoPos(i);
				for (int j = 0; j < ruta.getlistaTrips().size(); j++) 
				{
					VOViaje viaje = ruta.getlistaTrips().darElementoPos(j);
					boolean origen=false;
					boolean destino=false;
					if (viaje.getHora()==Integer.parseInt(hora[0]))
					{
						for (int k = 0; k < ruta.getlistaParadas().size(); k++) 
						{
							VOParada parada = ruta.getlistaParadas().darElementoPos(k);
							if (parada.getStopId()==Integer.parseInt(idOrigen))
							{
								origen=true;
							}
							if (parada.getStopId() == Integer.parseInt(idDestino))
							{
								destino=true;
							}
						}
						if (origen && destino)
						{
							viajes.addFirst(viaje);
						}
					}
				}

			}
			return viajes;
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public VORuta ITSrutaMenorRetardo(String fecha) 
	{
		// TODO Auto-generated method stub
		
		return null;
	}

	@Override
	public IList<VOServicio> ITSserviciosMayorDistancia(String fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VORetardo> ITSretardosViaje(String fecha, String idViaje) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOParada> ITSparadasCompartidas(String fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public boolean rutaDisponibleEnFecha(VORuta pRuta,String fecha)
	{
		boolean disponible= false;
		try 
		{	
			for (int j = 0; j < pRuta.getlistaTrips().size() &&!disponible; j++) 
			{
				VOViaje viaje= pRuta.getlistaTrips().darElementoPos(j);
				if(toDate(viaje.getCalendar()[8]).before(toDate(fecha)) && toDate(viaje.getCalendar()[9]).after(toDate(fecha)) &&Integer.parseInt(viaje.getDateException())!=Integer.parseInt(fecha))
				{
					disponible=true;
				}
			}
		} 
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return disponible;
	}

	//------------------------------------------------------------------------------------------------------------------------------	
	//------------------------------------------------------------------------------------------------------------------------------	
	//M�todos de carga para los archivos est�ticos
	//------------------------------------------------------------------------------------------------------------------------------
	//------------------------------------------------------------------------------------------------------------------------------

	private void loadRoutes(String rOUTES2) 
	{
		try 
		{
			loadStops(STOPS);			
			listaRoutes= new DoubleLinkedList<VORuta>();
			FileReader reader = new FileReader(ROUTES);
			BufferedReader br = new BufferedReader(reader);
			String linea=br.readLine();
			while((linea = br.readLine())!=null)
			{
				String[] info=linea.split(",");
				VORuta elemento= new VORuta(info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8]);
				listaRoutes.addLast(elemento);				
			}
			br.close();

			loadTrips(TRIPS, listaRoutes);
			loadCalendarExDates(CALENDAR_DATES, listaRoutes);
			loadCalendar(CALENDAR, listaRoutes);
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e1)
		{
			e1.printStackTrace();
		}

	}
	private void loadStops(String sTOPS2) 
	{
		FileReader reader;
		try 
		{
			listaStops= new DoubleLinkedList<VOParada>();
			reader = new FileReader(STOPS);
			BufferedReader br = new BufferedReader(reader);
			String linea=br.readLine();

			while((linea=br.readLine())!=null)
			{
				String[] split=linea.split(",");
				VOParada elemento = new VOParada(split[0], split[1], split[2], split[3], split[4], split[5], split[6], split[7], split[8]);
				listaStops.addFirst(elemento);
			}
			br.close();
			//loadStopTimes(STOP_TIMES, listaStops);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void loadStopTimes(String stopTimesFile, DoubleLinkedList<VORuta> listaRuta) 
	{
		FileReader reader;
		try 
		{
			listaStopTimes = new DoubleLinkedList<>();
			reader = new FileReader(stopTimesFile);
			BufferedReader br = new BufferedReader(reader);
			String linea=br.readLine();

			while((linea=br.readLine())!=null)
			{
				String[] split=linea.split(",");
				VOStopTimes elemento = new  VOStopTimes(split[0], split[1], split[2], split[3], split[4], split[5], split[6], split[7]);	
				listaStopTimes.addLast(elemento);
				for (int i = 0; i < listaStops.size(); i++) 
				{
					VOParada parada =listaStops.darElementoPos(i);
					for (int j = 0; j < listaStopTimes.size(); j++) 
					{
						VOStopTimes stopT = listaStopTimes.darElementoPos(j);
						if(parada.getStopId()==Integer.parseInt(stopT.getStopId()))
						{
							parada = listaStops.darElementoPos(i);
							stopT.setParada(parada);
						}
					}
					for (int k = 0; k < listaRuta.size(); k++) 
					{
						VORuta ruta = listaRuta.darElementoPos(k);
						for (int j = 0; j < ruta.getlistaTrips().size(); j++) 
						{
							VOViaje viaje = ruta.getlistaTrips().darElementoPos(j);
							if(Integer.parseInt(viaje.getIdViaje())==elemento.getTripId())
							{
								viaje.getStopTimes().addLast(elemento);
								if(!ruta.getlistaParadas().repeatedElement(parada))
									ruta.getlistaParadas().addLast(parada);
								if(!parada.getRutas().repeatedElement(ruta))
									parada.getRutas().addLast(ruta);
								if(!parada.getListaViajes().repeatedElement(viaje))
								parada.getListaViajes().addLast(viaje);							
							}
						}
					}
				}
				
			}
			br.close();	
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void loadTrips(String tRIPS2, DoubleLinkedList<VORuta> listaRutas) {
		FileReader reader;
		try 
		{
			listaViajes = new DoubleLinkedList<VOViaje>();
			reader = new FileReader(TRIPS);
			BufferedReader br = new BufferedReader(reader);
			String linea=br.readLine();
			while((linea=br.readLine())!=null)
			{
				String[] split=linea.split(",");

				VOViaje elemento = new VOViaje(split[0], split[1], split[2], split[3], split[4], split[5], split[6], split[7], split[8], split[9]);
				listaViajes.addLast(elemento);
			}
			br.close();

			for (int i =0; i<listaRutas.size();i++)
			{
				VORuta ruta = listaRutas.darElementoPos(i);
				for (int j = 0; j < listaViajes.size(); j++) 
				{
					VOViaje viaje =listaViajes.darElementoPos(i);
					if(Integer.parseInt(ruta.getIdRoute())== Integer.parseInt(viaje.getRouteId())&& ruta.getlistaTrips().repeatedElement(viaje))
					{
						ruta.getlistaTrips().addLast(viaje);
					}
				}
			}
			loadStopTimes(STOP_TIMES,listaRutas);
			loadTransfers();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	//Se van a agregar las fechas de servicio a las rutas.
	private void loadCalendarExDates(String calendarDatesFile, DoubleLinkedList<VORuta> listaRutas) 
	{
		FileReader reader;

		try 
		{
			reader = new FileReader(calendarDatesFile);
			BufferedReader br = new BufferedReader(reader);
			String linea=br.readLine();

			while((linea=br.readLine())!=null)
			{
				String[] split=linea.split(",");

				for (int j = 0; j < listaViajes.size(); j++) 
				{
					VOViaje viaje= listaViajes.darElementoPos(j);
					if(Integer.parseInt(viaje.getServiceId())== Integer.parseInt(split[0]))//split[0]=seviceId
					{
						viaje.setDateException(split[1]);
					}
				}
			}
			br.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void loadCalendar(String calendarDatesFile, DoubleLinkedList<VORuta> listaRutas) 
	{
		FileReader readerC;
		try 
		{
			readerC=new FileReader (CALENDAR);
			BufferedReader brC = new BufferedReader(readerC);
			String lineaC=brC.readLine();

			while((lineaC=brC.readLine())!=null)
			{
				String[] calendario=new String[10];
				String[] splitC =lineaC.split(",");

				for (int i = 1; i < splitC.length; i++) 
				{
					calendario[i]=splitC[i];
				}

				for (int j = 0; j < listaViajes.size(); j++) 
				{
					VOViaje viaje= listaViajes.darElementoPos(j);
					if(Integer.parseInt(viaje.getServiceId())== Integer.parseInt(calendario[0]))//split[0]=seviceId
					{
						viaje.setCalendar(calendario);
					}
				}

			}
			brC.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void loadTransfers()
	{
		FileReader reader;
		try 
		{
			reader=new FileReader (TRANSFERS);
			BufferedReader br = new BufferedReader(reader);
			String linea=br.readLine();

			while((linea=br.readLine())!=null)
			{
				String[] split =linea.split(",");
				VOTransfer elemento = new VOTransfer(Integer.parseInt(split[3]));

				for (int i = 0; i < listaStops.size(); i++) 
				{
					VOParada parada= listaStops.darElementoPos(i);
					if(parada.getStopId()==Integer.parseInt(split[0]) ||parada.getStopId()==Integer.parseInt(split[1]))
					{
						parada.getListaTransbordos().addLast(elemento);
						elemento.getListadeParadas().addLast(parada);
					}
				}
			}
			br.close();
		}
		catch (Exception e) 
		{
			// TODO: handle exception
		}
	}

	//------------------------------------------------------------------------------------------------------------------------------	
	//------------------------------------------------------------------------------------------------------------------------------	
	//M�todos de carga para los archivos en tiempo real
	//------------------------------------------------------------------------------------------------------------------------------
	//------------------------------------------------------------------------------------------------------------------------------

	public void readBusUpdate(File rtFile) throws Exception
	{
		Gson gson = new Gson();
		BufferedReader br = null;
		try
		{
			br= new BufferedReader(new FileReader(rtFile));
			busUpdates= gson.fromJson(br.readLine(), BusUpdateVO[].class);

			for (int i = 0; i < listaRoutes.size(); i++) 
			{
				VORuta ruta = listaRoutes.darElementoPos(i);
				for (int j = 0; j < busUpdates.length; j++) 
				{
					if(ruta.getIdRoute().equals(busUpdates[j].getRouteNo()))
					{
						ruta.getlistaBus().addLast(busUpdates[j]);
					}
				}
			}

			for (int i = 0; i < listaViajes.size(); i++) 
			{
				VOViaje viaje = listaViajes.darElementoPos(i);
				for (int j = 0; j < busUpdates.length; j++) 
				{
					if(viaje.getIdViaje().equals(busUpdates[j].getTripId()))
					{
						viaje.getBusUpdates().addLast(busUpdates[j]);
					}
				}
			}

			for (int i = 0; i < listaViajes.size(); i++) 
			{
				VOViaje viaje = listaViajes.darElementoPos(i);
				for (int j = 0; j < viaje.getStopTimes().size(); j++) 
				{
					VOStopTimes stopT = viaje.getStopTimes().darElementoPos(j);
					for (int k = 0; k < viaje.getBusUpdates().size(); k++)  
					{
						BusUpdateVO busU = viaje.getBusUpdates().darElementoPos(k);
						if(getDistance(Double.parseDouble(stopT.getParada().getStopLat()), Double.parseDouble(stopT.getParada().getStopLon()), Double.parseDouble(busU.getLatitutde()), Double.parseDouble(busU.getLongitude()))<=70)
						{
							int schTime= toTimeInSec(stopT.getArrivalTime());

							int actTime= toTimeInSec(busU.getRecordedTime());
							String time= busU.getRecordedTime();
							viaje.setHora(time);

							if((actTime-schTime)>120)
							{
								VORetardo retardo = new VORetardo(Integer.parseInt(busU.getTripId()), Integer.parseInt(stopT.getStopId()), actTime-schTime);
								stopT.getParada().getListaRetardos().addLast(retardo);
								stopT.getParada().setNumeroIncidentes(stopT.getParada().getNumeroIncidentes()+1);
								viaje.setRetardo(true);
								viaje.setTiempoRetardo(actTime-schTime);
								viaje.setHoraRetardo(time);
							}
						}
					}
				}
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				br.close();
			}
			catch(IOException e1)
			{
				e1.printStackTrace();
			}			
		}
	}

	public void readStopEstimService(File rtFile) throws Exception 
	{
		BufferedReader br = null;
		try
		{
			br= new BufferedReader(new FileReader(rtFile));
			StopEstimService[] stopE= new Gson().fromJson(br.readLine(), StopEstimService[].class);

			for (int i=0; i<stopE.length;i++)
			{
				StopEstimService stop = stopE[i];
				String routeN=stop.getRouteNo();				
				for(int j=0;j<listaRoutes.size();j++)
				{
					VORuta ruta = listaRoutes.darElementoPos(j);
					if(routeN.equals(ruta.getShortName()))
					{
						ruta.getlistaStopEstim().addLast(stop);
					}
				}
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				br.close();
			}
			catch(IOException e1)
			{
				e1.printStackTrace();
			}			
		}
	}
	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
		final int R = 6371*1000; // Radious of the earth
		double latDistance = toRad(lat2-lat1);
		double lonDistance = toRad(lon2-lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double distance = R * c;
		return distance;
	}

	private double toRad(double value) {return value * Math.PI / 180;}

	private int toTimeInSec(String s)
	{
		String t= s.substring(0,8);
		String[] split= t.split(":");

		int hours = (s.contains("am"))?Integer.parseInt(split[0]):Integer.parseInt(split[0])+12;
		int minutes = Integer.parseInt(split[1]);
		int secs = Integer.parseInt(split[2]);

		return secs+60*minutes+60*60*hours;
	}

	private Date toDate(String date)
	{
		DateFormat formato=new SimpleDateFormat("yy-mm-dd");

		try
		{
			java.sql.Date fecha = (Date)formato.parse(date);
			return fecha;
		} 
		catch (ParseException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
