package model.data_structures;

import java.util.Iterator;

import model.data_structures.QueueList.Node;

public class Stack<T> implements IList<T>, Iterable<T>
{
	private int size;
	private Node<T> first;

	public static class Node <T>
	{
		private T element;
		private Node<T> next;
		private Node<T> prev;

		public Node(T e)
		{
			element= e;
			next=null;
			prev=null;
		}

		public Node<T> darSiguiente()
		{
			return this.next;
		}
		public Node<T> darAnterior()
		{
			return this.prev;
		}

		public T darElemento()
		{
			return this.element;
		}
	}

	public Stack()
	{
		size=0;
		first=null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public T getFirst() {
		// TODO Auto-generated method stub
		return  first.element;
	}

	@Override
	public T getLast() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addFirst(T elemento) {
		// TODO Auto-generated method stub
		Node n=new Node<>(elemento);

		if(first==null)
		{
			first= n;
		}
		else 
		{
			n.next=first;
			first=n;
		}
		size++;
	}

	@Override
	public void addLast(T elemento) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addPos(T elemento, int pos) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public T darElementoPos(int pos) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T removePos(int pos) throws Exception {
		// TODO Auto-generated method stub
		if(pos==0)
		{
			Node<T> n=first;
			first=first.next;
			size--;
			return n.element;
		}
		return null;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size==0?true:false;
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() 
		{
			private Node<T> current= first;

			@Override
			public boolean hasNext() 
			{
				// TODO Auto-generated method stub
				return current!=null;
			}

			@Override
			public T next() 
			{
				// TODO Auto-generated method stub
				Node<T> result = null;
		        if(current != null) {
		          result = (Node<T>) current;
		          current = current.darSiguiente();
		        }
		        return (T) result;
			}
			
		};
	}
}
