package model.data_structures;

import java.util.Iterator;

public class QueueList<T> implements IList<T>, Iterable<T>{
	
	public static class Node <T>
	{
		private T element;
		private Node<T> next;
		private Node<T> prev;

		public Node(T e)
		{
			element= e;
			next=null;
			prev=null;
		}

		public Node<T> darSiguiente()
		{
			return this.next;
		}
		public Node<T> darAnterior()
		{
			return this.prev;
		}
		public T darElemento()
		{
			return this.element;
		}
	}
	
	private int size;
	private Node<T> first;
	private Node<T> last;
	
	public QueueList() 
	{
		size=0;
		first=null;
		last=null;
	} 
	
	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() 
		{
			private Node<T> current= first;

			@Override
			public boolean hasNext() 
			{
				// TODO Auto-generated method stub
				return current!=null;
			}

			@Override
			public T next() 
			{
				// TODO Auto-generated method stub
				Node<T> result = null;
		        if(current != null) {
		          result = (Node<T>) current;
		          current = current.darSiguiente();
		        }
		        return (T) result;
			}
			
		};
	}
	
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public T getFirst() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T getLast() {
		// TODO Auto-generated method stub
		return last.element;
	}

	@Override
	public void addFirst(T elemento) {
		// TODO Auto-generated method stub
		Node<T> nodo= new Node(elemento);
		if (size==0)
		{
			first= nodo;
			last=nodo;
		}
		else
		{
			nodo.next=first;
			first=nodo;
		}
		size++;
	}

	@Override
	public void addLast(T elemento) {
		// TODO Auto-generated method stub
				
	}

	@Override
	public void addPos(T elemento, int pos) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public T darElementoPos(int pos) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T removePos(int pos) throws Exception {
		// TODO Auto-generated method stub
		if (pos==size-1)
		{
			Node prev= last.prev;
			Node aRemover=last;
			prev.next=null;
			last=prev;
			size--;
			return (T) aRemover.element;
		}
		else
		{
			return null;
		}
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		boolean vacio = false;
		if (size==0)
		{
			vacio=true;
		}
		return vacio;
	}
}