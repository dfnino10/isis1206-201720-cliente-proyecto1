package model.vo;

import java.sql.Time;

import model.data_structures.DoubleLinkedList;

public class VOViaje{

	private String routeId, serviceId, tripId, tripHeadSign, tripShortName, directionId, blockId, shapeId, wheelchairAccessible, bikesAllowed, dateException;
	private String[] calendar;
	DoubleLinkedList<VOStopTimes> stopTimes;
	DoubleLinkedList<BusUpdateVO> busUpdates;
	private boolean retardo;
	private int tiempoRetardo;
	private int horaRetardo;
	private int hora;

	public VOViaje(String route_id, String service_id, String trip_id, String trip_headsign, String trip_short_name, String direction_id, String block_id, String shape_id, String wheelchair_accessible, String bikes_allowed)
	{
		routeId=route_id;
		serviceId=service_id;
		tripId=trip_id;
		tripHeadSign=trip_headsign;
		tripShortName=trip_short_name;
		directionId=direction_id;
		blockId=block_id;
		shapeId=shape_id;
		wheelchairAccessible=wheelchair_accessible;
		bikesAllowed=bikes_allowed;
		calendar=null;
		stopTimes = new DoubleLinkedList<VOStopTimes>();
		busUpdates= new DoubleLinkedList<BusUpdateVO>();
		dateException=null;
		retardo=false;
		tiempoRetardo=0;
		horaRetardo=0;
	}

	public boolean isRetardo() {
		return retardo;
	}

	public void setRetardo(boolean retardo) {
		this.retardo = retardo;
	}
	

	public void setHora(String time)
	{
		String t= time.substring(0,8);
		String[] split= t.split(":");

		int hours = (time.contains("am"))?Integer.parseInt(split[0]):Integer.parseInt(split[0])+12;
		int minutes = Integer.parseInt(split[1]);
		int secs = Integer.parseInt(split[2]);
		this.hora= hours;
	}
	
	public void setTiempoRetardo(int retardo) {
		this.tiempoRetardo = retardo;
	}
	
	public void setHoraRetardo(String retardo) {
		//Ej retardo: "03:46:20 pm"
		String t= retardo.substring(0,8);
		String[] split= t.split(":");

		int hours = (retardo.contains("am"))?Integer.parseInt(split[0]):Integer.parseInt(split[0])+12;
		this.horaRetardo = hours;
	}
	
	public int getHora(){return hora;}
	public int getHoraRetardo(){return horaRetardo;}
	public int getTiempoRetardo(){return tiempoRetardo;}
	public String getRouteId(){return routeId;}
	public String getServiceId(){return serviceId;}
	public String getTripHeadSign(){return tripHeadSign;}
	public String getTripShortName(){return tripShortName;}
	public String getDirectionId(){return directionId;}
	public String getBlockId(){return blockId;}
	public String getShapeId(){return shapeId;}
	public String getWheelChairAccessible(){return wheelchairAccessible;}
	public String getBikesAllowed(){return bikesAllowed;}
	public DoubleLinkedList<VOStopTimes> getStopTimes(){return stopTimes;}
	
	public DoubleLinkedList<BusUpdateVO> getBusUpdates() {return busUpdates;}

	public void setBusUpdates(DoubleLinkedList<BusUpdateVO> busUpdates) {
		this.busUpdates = busUpdates;
	}

	public String getDayCalendar(int day)
	{
		String yesOno =calendar[day];
		return yesOno;
	}

	public String getIdViaje() {return tripId;}
	
	public void setCalendar(String[] calendario){calendar=calendario;}
	public String[] getCalendar(){return calendar;}
	public void setDateException(String dateException)
	{
		this.dateException = dateException;
	}
	public String getDateException()
	{
		return dateException;
	}


}
