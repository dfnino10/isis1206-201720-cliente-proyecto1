package model.vo;

public class VORetardoViaje {
	
	private int viajeid;
	private int tiempoRetardo;
	
	public VORetardoViaje (int viaje, int tiempoR)
	{
		viajeid=viaje;
		tiempoRetardo=tiempoR;
	}
	
	public int getViajeid() {
		return viajeid;
	}
	public void setViajeid(int viajeid) {
		this.viajeid = viajeid;
	}
	public int getTiempoRetardo() {
		return tiempoRetardo;
	}
	public void setTiempoRetardo(int tiempoRetardo) {
		this.tiempoRetardo = tiempoRetardo;
	}

}
