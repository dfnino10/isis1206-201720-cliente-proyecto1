package model.vo;

import model.data_structures.IList;

public class VORangoHora {
	
	private int horaInicial;
	private int horaFinal;
	
	/**
	 * Lista de retardos ordenados por tiempo total de retardo
	 */
	IList<VORetardoViaje> listaRetardos;

	public int getHoraInicial() {
		return horaInicial;
	}

	public void setHoraInicial(int horaInicial) {
		this.horaInicial = horaInicial;
	}

	public int getHoraFinal() {
		return horaFinal;
	}

	public void setHoraFinal(int horaFinal) {
		this.horaFinal = horaFinal;
	}

	public IList<VORetardoViaje> getListaRetardos() {
		return listaRetardos;
	}

	public void setListaRetardos(IList<VORetardoViaje> listaRetardos) {
		this.listaRetardos = listaRetardos;
	}
}
