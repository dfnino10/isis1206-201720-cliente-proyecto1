package model.vo;

import model.data_structures.DoubleLinkedList;

/**
 * Representation of a  object
 */
public class VORuta {

	private String id;
	private String agencyid;
	private String shortName;
	private String longName;
	private String desc;
	private String type;
	private String url;
	private String color;
	private String textColor;
	private DoubleLinkedList<VOViaje> listaTrips; 
	private DoubleLinkedList<BusUpdateVO> listaBusUpdate; 
	private DoubleLinkedList<StopEstimService> listaStopEstim;
	private DoubleLinkedList<VOParada> listaParadas; 
	
	public VORuta(String route_id,String agency_id,String route_short_name,String route_long_name,String route_desc,String route_type,String route_url,String route_color,String route_text_color)
	{	
		id= route_id;
		agencyid= agency_id;
		shortName= route_short_name;
		longName= route_long_name;
		desc=route_desc;
		type= route_type;
		url= route_url;
		color= route_color;
		textColor= route_text_color;
		listaTrips=new DoubleLinkedList<VOViaje>();
		listaBusUpdate= new DoubleLinkedList<BusUpdateVO>();
		listaStopEstim= new DoubleLinkedList<StopEstimService>();
		listaParadas = new DoubleLinkedList<VOParada>();
	}
	public DoubleLinkedList<VOViaje> getlistaTrips()
	{
		return listaTrips;
	}
	
	public DoubleLinkedList<BusUpdateVO> getlistaBus()
	{
		return listaBusUpdate;
	}
	
	public DoubleLinkedList<StopEstimService> getlistaStopEstim()
	{
		return listaStopEstim;
	}
	
	public DoubleLinkedList<VOParada> getlistaParadas()
	{
		return listaParadas;
	}
	
	public String getAgencyId()
	{
		return agencyid;
	}

	/**
	 * @return name -  name
	 */
	public String getShortName()
	{
		return shortName;
	}
	
	public String getLongName()
	{
		return longName;
	}
	
	public String getDesc()
	{
		return desc;
	}

	public String getType()
	{
		return type;
	}
	
	public String getUrl()
	{
		return url;
	}
	
	public String getColor()
	{
		return color;
	}
	
	public String getTextColor()
	{
		return textColor;
	}
	
	/**
	 * @return id - 's id number
	 */
	public String getIdRoute() {
		// TODO Auto-generated method stub
		return id;
	}
}
