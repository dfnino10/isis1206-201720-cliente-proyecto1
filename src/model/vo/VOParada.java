package model.vo;

import model.data_structures.DoubleLinkedList;

/**
 * Representation of a Stop object
 */
public class VOParada 
{
	private String stopId, stopCode, stopName, stopDesc, stopLat, stopLon, zoneId, stopUrl, locationType, parentStation; 

	/**
	 * Modela el n�mero de incidentes: "El numero de buses retrasados en esta parada"
	 */
	private int numeroIncidentes;

	private DoubleLinkedList<VORetardo> listaRetardos;
	
	private DoubleLinkedList<VORuta> rutas;
	
	private DoubleLinkedList<VOTransfer> listaTransbordos;
	
	private DoubleLinkedList<VOViaje> listaViajes;

	public VOParada(String stop_id,String stop_code,String stop_name,String stop_desc, String stop_lat, String stop_lon, String zone_id, String stop_url,String location_type)
	{
		stopId=stop_id;
		stopCode=stop_code;
		stopName=stop_name;
		stopDesc=stop_desc;
		stopLat=stop_lat;
		stopLon=stop_lon;
		zoneId=zone_id;
		stopUrl=stop_url;
		locationType=location_type;
		parentStation=null;
		listaRetardos=new DoubleLinkedList<VORetardo>();
		rutas= new DoubleLinkedList<VORuta>();
		listaTransbordos = new DoubleLinkedList<VOTransfer>();
		listaViajes= new DoubleLinkedList<VOViaje>();
		numeroIncidentes=0;
	}

	public DoubleLinkedList<VOViaje> getListaViajes() {
		return listaViajes;
	}
	public void setListaViajes(DoubleLinkedList<VOViaje> listaViajes) {
		this.listaViajes = listaViajes;
	}
	public DoubleLinkedList<VOTransfer> getListaTransbordos() {
		return listaTransbordos;
	}
	public void setListaTransbordos(DoubleLinkedList<VOTransfer> listaTransbordos) {
		this.listaTransbordos = listaTransbordos;
	}
	public DoubleLinkedList<VORuta> getRutas() {
		return rutas;
	}
	public void setRutas(DoubleLinkedList<VORuta> rutas) {
		this.rutas = rutas;
	}
	public DoubleLinkedList<VORetardo> getListaRetardos() {
		return listaRetardos;
	}
	public void setListaRetardos(DoubleLinkedList<VORetardo> listaRetardos) {
		this.listaRetardos = listaRetardos;
	}
	/**
	 * @return id - stop's id
	 */
	public int getStopId() 
	{
		// TODO Auto-generated method stub
		return Integer.parseInt(stopId);
	}
//	public DoubleLinkedList<VOStopTimes> getListaStopTimes() 
//	{
//		return listaStopTimes;
//	}

	/**
	 * @return name - stop name
	 */
	public String getStopName() 
	{
		// TODO Auto-generated method stub
		return stopName;
	}

	public String getStopCode()
	{
		return stopCode;
	}
	public String getStopDesc()
	{
		return stopDesc;
	}
	public String getStopLat()
	{
		return stopLat;
	}
	public String getStopLon()
	{
		return stopLon;
	}
	public String getZoneId()
	{
		return zoneId;
	}
	public String getStopUrl()
	{
		return stopUrl;
	}
	public String getLocationType()
	{
		return locationType;
	}
	public String getParentStation()
	{
		return parentStation;
	}
	public int getNumeroIncidentes() 
	{
		return numeroIncidentes;
	}

	/**
	 * @param numeroIncidentes the numeroIncidentes to set
	 */
	public void setNumeroIncidentes(int numeroIncidentes)
	{
		this.numeroIncidentes = numeroIncidentes;

	}

}

