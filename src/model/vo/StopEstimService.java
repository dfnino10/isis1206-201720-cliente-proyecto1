package model.vo;

import model.vo.BusUpdateVO.RouteMap;

public class StopEstimService
{
	private String RouteNo,RouteName,Direction;
	
	private RouteMap RouteMap;
	
	private Schedules[] Schedules;
	
	public class Schedules
	{
		private String Pattern, Destination,ExpectedLeaveTime,ExpectedCountdown,ScheduleStatus,LastUpdate;
		private boolean CancelledTrip,CancelledStop,AddedTrip,AddedStop;
	}

	public String getRouteNo() {
		return RouteNo;
	}

	public void setRouteNo(String routeNo) {
		RouteNo = routeNo;
	}

	public String getRouteName() {
		return RouteName;
	}

	public void setRouteName(String routeName) {
		RouteName = routeName;
	}

	public String getDirection() {
		return Direction;
	}

	public void setDirection(String direction) {
		Direction = direction;
	}

	public RouteMap getRouteMap() {
		return RouteMap;
	}

	public void setRouteMap(RouteMap routeMap) {
		RouteMap = routeMap;
	}

	public Schedules[] getSchedules() {
		return Schedules;
	}

	public void setSchedules(Schedules[] schedules) {
		Schedules = schedules;
	}
}
