package API;

import model.data_structures.IList;
import model.vo.VOParada;
import model.vo.VORetardo;
import model.vo.VORuta;
import model.vo.VOServicio;
import model.vo.VOPlan;
import model.vo.VORangoHora;
import model.vo.VOTransfer;
import model.vo.VOViaje;

public interface ISTSManager 
{
	/**
	 * inicializa estrucuturas de datos
	 */
	void ITSInit(); 

	/**
	 * Carga toda la informaci�n est�tica necesaria para la operaci�n del sistema.
	 *(Archivo de rutas, viajes, paradas, etc.)
	 */
	void ITScargarGTFS();

	/**
	 * Carga la informaci�n en tiempo real de los buses en para fecha determinada.
	 * @param fecha
	 */
	void ITScargarTR(String fecha);

	/**
	 * Retorna una IList de rutas que son prestadas por una empresa determinada, en
	 * una fecha determinada.
	 * @param nombreEmpresa
	 * @param fecha
	 * @return IList de rutas.
	 */
	IList<VORuta>ITSrutasPorEmpresa(String nombreEmpresa, String fecha) throws Exception;

	/**
	 * Retorna una IList de viajes retrasados para una ruta en una fecha determinada.
	 * @param idRuta
	 * @param fecha
	 * @return
	 */
	IList<VOViaje> ITSviajesRetrasadosRuta(String idRuta, String fecha);

	/**
	 * Retorna una lista de Paradas en las que hubo un retraso en una fecha dada
	 * @param fecha
	 * @return
	 */
	IList<VOParada> ITSparadasRetrasadasFecha( String fecha);

	/**
	 * Retorna una lista de paradas, las cuales conforman todos los posibles transbordos 
	 * que se derivan a partir de la primera parada de dicha ruta.
	 * @param idRuta
	 * @param fecha
	 * @return
	 */
	IList<VOTransfer> ITStransbordosRuta(String idRuta, String fecha);

	/**
	 *Dada una secuencia de paradas, para cada una de ellas se calculan las rutas y los viajes
	 *que tienen planeado utilizarla en una fecha dada dentro de un rango de horas. Debe
	 *procesar todas las paradas solicitadas en el orden en el que le fueron dadas (utilice una
	 *cola). Retorna una lista de paradas, en donde cada parada tiene una lista de rutas 
	 *(ordenadas por identificador sin repeticiones) y cada ruta tiene una lista de viajes (ordenados por hora de
	 *parada) de la parada que se est� analizando.
	 * @param fecha
	 * @param horaInicio
	 * @param horaFin
	 * @param idsDeParadas, secuencia de paradas
	 * @return plan
	 */
	VOPlan ITSrutasPlanUtilizacion (IList<String> idsDeParadas,String fecha, String horaInicio, String horaFin);

	/**
	 * Retorna una lista de rutas que son prestadas por una empresa determinada, en una fecha determinada.
	 *  La lista est� ordenada por cantidad total de paradas.
	 * @param nombreEmpresa
	 * @param fecha
	 * @return
	 * @throws Exception 
	 */
	IList<VORuta>ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha) throws Exception;

	/**
	 * Retornar una lista con todos los viajes en los que despu�s de un retardo, todas las 
	 * paradas siguientes tuvieron retardo, para una ruta espec�fica en una fecha espec�fica. 
	 * Se debe retornar una lista ordenada por el id del viaje, y la localizaci�n de las 
	 * paradas, de mayor a menor en tiempo de retardo.
	 * @param idRuta
	 * @param fecha
	 * @return
	 */
	IList<VOViaje> ITSviajesRetrasoTotalRuta(String idRuta, String fecha);

	/**
	 * Retorna una lista ordenada con rangos de hora en los que mas retardos hubo.
	 * @param idRuta
	 * @param Fecha
	 * @return
	 * @throws Exception 
	 */
	VORangoHora ITSretardoHoraRuta (String idRuta, String Fecha ) throws Exception;

	/**
	 * Retorna una lista con los viajes para ir de la parada de inicio a la parada 
	 * final en una fecha y franja horaria determinada.
	 * @param idOrigen
	 * @param idDestino
	 * @param fecha
	 * @param horaInicio
	 * @param horaFin
	 * @return
	 * @throws Exception 
	 */
	IList <VOViaje> ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaInicio, String horaFin) throws Exception;

	/**
	 * Retorna la ruta que tiene menos retardo promedio en distancia de sus viajes 
	 * en una fecha dada. El retardo promedio en distancia de un viaje corresponde
	 * al total de los tiempos de retardo de sus paradas dividido por la distancia recorrida.
	 * @param idRuta
	 * @param fecha
	 * @return
	 */
	VORuta ITSrutaMenorRetardo (String fecha);

	/**
	 * Retorna una lista con los servicios que mas distancia recorren en una
	 * fecha. La lista est� ordenada por distancia.
	 * @param fecha
	 * @return
	 */
	IList<VOServicio> ITSserviciosMayorDistancia(String fecha);

	/**
	 * Retorna una lista con todos los retardos de un viaje en una fecha dada.
	 * @param fecha
	 * @param idViaje
	 * @return
	 */
	IList<VORetardo> ITSretardosViaje (String fecha, String idViaje);

	/**
	 * Retorna una lista con todas las paradas del sistema que son compartidas 
	 * por mas de una ruta en una fecha determinada.
	 * @param fecha
	 * @return
	 */
	IList <VOParada>ITSparadasCompartidas (String fecha);
}
