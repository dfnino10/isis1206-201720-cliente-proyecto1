package controller;

import API.ISTSManager;
import model.data_structures.IList;
import model.exceptions.DateNotFoundExpection;
import model.logic.STSManager;
import model.vo.VOParada;
import model.vo.VORetardo;
import model.vo.VORuta;
import model.vo.VOServicio;
import model.vo.VOPlan;
import model.vo.VORangoHora;
import model.vo.VOTransfer;
import model.vo.VOViaje;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ISTSManager manager  = new STSManager();

	/**
	 * inicializa estructuras
	 */
	public static void ITSInit() 
	{
		manager.ITSInit();	
	}

	/**
	 * C1
	 */
	public static void ITScargarGTFS() 
	{
		manager.ITScargarGTFS();	
	}

	/**
	 * C1
	 * @param fecha
	 */
	public static void ITScargarTR(String fecha) throws DateNotFoundExpection
	{
		manager.ITScargarTR(fecha);
	}

	/**
	 * A1
	 */
	public static IList<VORuta> ITSrutasPorEmpresa(String nombreEmpresa, String fecha) throws Exception
	{
		return manager.ITSrutasPorEmpresa(nombreEmpresa, fecha);
	}

	/**
	 * A2
	 */
	public static IList<VOViaje> ITSviajesRetrasadosRuta(String idRuta, String fecha) 
	{
		return manager.ITSviajesRetrasadosRuta(idRuta, fecha);
	}

	/**
	 * A3
	 */
	public static IList<VOParada> ITSparadasRetrasadasFecha(String fecha) 
	{
		return manager.ITSparadasRetrasadasFecha(fecha);
	}

	/**
	 * A4
	 */
	public static IList<VOTransfer> ITStransbordosRuta(String idRuta, String fecha)
	{
		return manager.ITStransbordosRuta(idRuta, fecha);
	}

	/**
	 * A5
	 */
	public static VOPlan ITSrutasPlanUtilizacion(IList<String> idsDeParadas, String fecha, String horaInicio, String horaFin) 
	{	
		return manager.ITSrutasPlanUtilizacion(idsDeParadas, fecha, horaInicio, horaFin);
	}

	/**
	 * B1
	 * @throws Exception 
	 */
	public static IList<VORuta> ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha) throws Exception
	{
		return manager.ITSrutasPorEmpresaParadas(nombreEmpresa, fecha);
	}

	/**
	 * B2
	 */
	public static IList<VOViaje> ITSviajesRetrasoTotalRuta(String idRuta, String fecha)
	{	
		return manager.ITSviajesRetrasoTotalRuta(idRuta, fecha);
	}

	/**
	 * B3
	 * @throws Exception 
	 */
	public static VORangoHora ITSretardoHoraRuta(String idRuta, String Fecha) throws Exception
	{
		return manager.ITSretardoHoraRuta(idRuta, Fecha);
	}

	/**
	 * B4
	 * @throws Exception 
	 */
	public static IList<VOViaje> ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaInicio, String horaFin) throws Exception 
	{ 		
		return manager.ITSbuscarViajesParadas(idOrigen, idDestino, fecha, horaInicio, horaFin);
	}

	/**
	 * B5
	 */
	public static VORuta ITSrutaMenorRetardo(String fecha) 
	{ 		
		return manager.ITSrutaMenorRetardo(fecha);
	}

	/**
	 * C2
	 */
	public static IList<VOServicio> ITSserviciosMayorDistancia(String fecha)
	{ 		
		return manager.ITSserviciosMayorDistancia(fecha);
	}

	/**
	 * C3
	 */
	public static IList<VORetardo> ITSretardosViaje(String fecha, String idViaje) 
	{		
		return manager.ITSretardosViaje(fecha, idViaje);
	}

	/**
	 * C4
	 */
	public static IList<VOParada> ITSparadasCompartidas(String fecha)
	{		
		return manager.ITSparadasCompartidas(fecha);
	}

}
